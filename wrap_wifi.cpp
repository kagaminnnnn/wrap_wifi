#include "wrap_wifi.hpp"

namespace WrapWiFiLib{
    void WrapWiFi::Setup(setup_type_t type,const char* ssid,const char* password){
        if(type == setup_type_t::ap_mode){
            WiFi.softAP(ssid,password);
            my_ip_address_ = WiFi.softAPIP();
        }else if(type == setup_type_t::sta_mode){
            WiFi.begin(ssid,password);
            while(WiFi.status() != WL_CONNECTED) {
                //wait connect...
                delay(100);
            }
            my_ip_address_ = WiFi.localIP();
        }
    }

    void WrapWiFi::SetupFixedIp(setup_type_t type,const char* ssid,const char* password,IPAddress my_ip,IPAddress subnet){
        if(type == setup_type_t::ap_mode){
            WiFi.softAP(ssid,password);
            my_ip_address_ = WiFi.softAPIP();
        }else if(type == setup_type_t::sta_mode){    
            WiFi.config(my_ip,my_ip,subnet);
            WiFi.begin(ssid,password);
            while(WiFi.status() != WL_CONNECTED) {
                //wait connect...
                delay(100);
            }
            my_ip_address_ = WiFi.localIP();
        }
    }

    void WrapWiFi::ReverceUdpPort(uint16_t port){
        udp_.push_back(WiFiUDP{});
        udp_[udp_.size() - 1].begin(port);
    }

    uint8_t WrapWiFi::UdpAvailable(uint8_t udp_num){
        return udp_[udp_num].parsePacket();
    }

    void WrapWiFi::UdpRead(uint8_t udp_num,uint8_t* read_data,uint8_t data_len){
        for(int i = 0;i < data_len;i++){
            read_data[i] = udp_[udp_num].read();
        }
    }

    void WrapWiFi::UdpWrite(uint8_t udp_num,const char* ip_address,uint16_t port,uint8_t* send_data,uint8_t data_len){
        udp_[udp_num].beginPacket(ip_address,port);
        udp_[udp_num].write(send_data,data_len);
        udp_[udp_num].endPacket();
    }
}
