#ifndef WRAP_WIFI_HPP
#define WRAP_WIFI_HPP

#include <vector>
#include <Arduino.h>
#include <WiFi.h>

namespace WrapWiFiLib{
    enum class setup_type_t{
        ap_mode = 0,
        sta_mode
    };

    class WrapWiFi{
        IPAddress my_ip_address_;
        std::vector<uint16_t> udp_port_;
        std::vector<WiFiUDP> udp_;
    public:
        void Setup(setup_type_t type,const char* ssid,const char* password);
        void SetupFixedIp(setup_type_t type,const char* ssid,const char* password,IPAddress my_ip,IPAddress subnet);
        
        IPAddress GetMyIpAddress()const{
            return my_ip_address_;
        }

        void ReverceUdpPort(uint16_t port);
        uint8_t UdpAvailable(uint8_t udp_num);
        void UdpRead(uint8_t udp_num,uint8_t* read_data,uint8_t data_len);
        void UdpWrite(uint8_t udp_num,const char* ip_address,uint16_t port,uint8_t* send_data,uint8_t data_len);
    };
}



#endif